const Order = require("../models/Order.js")
const Product = require("../models/Products.js")
const auth = require("../auth");

const User = require("../models/Users")


// Add order
module.exports.addToOrder = async (user, reqParams, reqBody) => {
  if (user.isAdmin) {
    return "Unauthorized";
  } else {
    let product = await Product.findById(reqParams.productId);

    let subTotal = reqBody.quantity * product.price;

    let newOrder = new Order({
      userId: user.id,
      product: [
        {
          productId: product.id,
          name: product.name,
          quantity: reqBody.quantity,
          price: product.price
        },
      ],
      subTotal: subTotal,
    });

    return newOrder.save().then(
    (order, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  }
};


// checkout
module.exports.getOrdersForAllUser = async () => {
  const orders = await Order.find({});
  if (orders) {
    return orders;
  } else {
    return false;
  }
};



// get specific order

module.exports.getOrdersForUser = async ( userId , isAdmin) => {
  console.log(isAdmin);

  if(!isAdmin){
    const orders = await Order.find({ userId  });
    if (orders) {
      return orders;
    } else {
      return false;
    }
  
    };

  // If the user is not admin, then return this message as a promise to avoid errors
  let message = Promise.resolve("You don't have the access rights to do this action.");

  return message.then((value) => {
    return value
  })
};