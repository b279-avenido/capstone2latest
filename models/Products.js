const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	
	name: {
		type: String,
		required : [true, "Name of product is required"]
	},
	description: {
		type: String,
		required : [true, "Description is required"]
	},
	price: {
		type: Number,
		required : [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required : [true, "Stocks is required"]
	},
	isActive: {
		
		type : Boolean,
		default : true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	// userOrders: [{
	// 	userId: {
	// 		type: String,
	// 		required : [true, "User ID is required"]
	// 	}
		/*orderId: {
			type: String,
			required : [true, "Order ID is required"]
		},*/
	// }]
	
});

module.exports = mongoose.model("PRODUCT", productSchema);