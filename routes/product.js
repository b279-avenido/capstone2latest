const express = require("express");
const router = express.Router()
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");
const User = require("../models/Users")




// creating product using admin
router.post("/", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// all product
router.get("/all", (req, res) => {

	productController.allProduct(req.body).then(resultFromController => res.send(resultFromController));
});

// router.get("/all", (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	productController.allProduct(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
// });



// get all active product
router.get("/active/all", (req, res) => {
	productController.allProductActive(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.allActive(isAdmin).then(resultFromController => res.send(resultFromController));
});






// get a single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getSingle(req.params).then(resultFromController => res.send(resultFromController));
});

// update product
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// delete or archiving product
router.put("/:productId/archive", auth.verify, (req, res) =>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.deleteProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

// remove product
router.put("/:productId/delete", auth.verify, (req, res) =>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});


// active product Admin only
router.get("/active/admin", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.allActiveAdmin(isAdmin).then(resultFromController => res.send(resultFromController));
});












module.exports = router;